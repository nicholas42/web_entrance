var LANGUAGE_CONFIG = {
    'Simplified' : {
        '0001' : '游戏载入中，请稍后',
        '0002' : '返回游戏列表',
        '0003' : '为了更好的体验，请将手机/平板横过来'
    },
    
    'Traditional' : {
        '0001' : '遊戲載入中，請稍後',
        '0002' : '返回游戲列表',
        '0003' : '為了更好的體驗，請將手機/平板橫過來'
    },

    'En' : {
        '0001' : 'The game is loading, please wait',
        '0002' : 'Return',
        '0003' : 'For a better experience, please turn your phone/tablet horizontally'
    },
        
    'Malay' : {
        '0001' : 'Permainan sedang dimuat, sila tunggu',
        '0002' : 'kembali',
        '0003' : 'Untuk pengalaman yang lebih baik, putar telefon/tablet anda secara mendatar'
    },

    'Thai' : {
        '0001' : 'กำลังโหลดเกมโปรดรอสักครู่',
        '0002' : 'กลับ',
        '0003' : 'เพื่อประสบการณ์ที่ดียิ่งขึ้นโปรดพลิกโทรศัพท์/แท็บเล็ตในแนวนอน'
    },

    'Vietnam' : {
        '0001' : 'Trò chơi đang tải, vui lòng đợi',
        '0002' : 'trở về',
        '0003' : 'Để có trải nghiệm tốt hơn, vui lòng xoay ngang điện thoại/máy tính bảng của bạn'
    },

    'Indonesia' : {
        '0001' : 'Game sedang dimuat, harap tunggu',
        '0002' : 'kembali',
        '0003' : 'Untuk pengalaman yang lebih baik, putar ponsel/tablet Anda secara horizontal'
    },

    'Hindi' : {
        '0001' : 'खेल लोड हो रहा है, कृपया प्रतीक्षा करें',
        '0002' : 'वापसी',
        '0003' : 'बेहतर अनुभव के लिए, कृपया अपना फ़ोन/टैबलेट क्षैतिज रूप से चालू करें'
    }
}

function localStr(_lang, _type){
    return LANGUAGE_CONFIG[_lang][_type];
}