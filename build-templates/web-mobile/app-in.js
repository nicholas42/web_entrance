// 禁用双指放大
document.documentElement.addEventListener('touchstart', function (event) {
    if (event.touches.length > 1) {
        event.preventDefault();
    }
}, {
    passive: false
});
 
// 禁用双击放大
var lastTouchEnd = 0;
document.documentElement.addEventListener('touchend', function (event) {
    var now = Date.now();
    if (now - lastTouchEnd <= 300) {
        event.preventDefault();
    }
    lastTouchEnd = now;
}, {
    passive: false
});

//解决iphoneX系列上部出现地址栏，可以滑动全屏后导致用户无法点击的问题
window.addEventListener('scroll', setOnScroll, false);

function setOnScroll(){
    var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
    if(scrollTop !=0 ){
        window.scrollTo(0, -100);  
    }
    var scrollLeft = document.documentElement.scrollLeft || document.body.scrollLeft;
    if(scrollLeft !=0 ){
        window.scrollTo(0, 0);  
    }
}

/**
 * @description 显示滑动全屏提示
 */
function showScroll2FullScreen(){
    if(window.innerHeight != document.documentElement.clientHeight){
        if (window.orientation == 90 || window.orientation == -90) { //横屏
            var scrollTop = document.documentElement.scrollTop || document.body.scrollTop;
            if(scrollTop !=0 ){
                window.scrollTo(0, -100);  
            } 
            document.getElementById('orientationswipe').style.display = 'block';
            window.removeEventListener('scroll', setOnScroll, false);
        }
    }else{
        document.getElementById('orientationswipe').style.display = 'none';
    }
}

/**
 * 初始化横屏提示页面
 */
function initMobileLandScapeTips(){
    alignLandScapeTips();
    //displayLandScapeDiv();
    //window.addEventListener('orientationchange', displayLandScapeDiv);

    var screenDirection = window.matchMedia("(orientation: portrait)");
    displayLandScapeDiv(screenDirection);
    
    screenDirection.addListener(displayLandScapeDiv); 
}

/**
 * 横竖屏判断
 */
function displayLandScapeDiv(e){
    if (e.matches){ //竖屏
        
        if(ENGINE_LOADING && ENGINE_LOADED && !ENTRANCE_SECNE_LOADED){
            setTimeout(() => {
                location.href = location.href;
            }, 500);
        }

        setTimeout(function () {
            alignLandScapeTips();
        }, 500);

        document.getElementById('landscape-tips-div').style.display = 'block';
        setAreaLimitImg('mb_no_service');

    }else{ //横屏

        document.getElementById('landscape-tips-div').style.display = 'none';
        setAreaLimitImg('pc_no_service');

        //解决isIPhoneX系列旋转后不适配的问题
        if(DEVICE.isSafari && DEVICE.isIPhoneXSerise() && window.cc){
            setTimeout(() => {
                window.scrollTo(0, -100);                    
            }, 1000);
        }

        //解决ios14旋转后不适配的问题
        if(DEVICE.iOSBigVersion() == 14 && !DEVICE.isIPhoneXSerise() && DEVICE.isSafari && window.cc){
            setTimeout(() => {
                window.scrollTo(0, -100);                    
            }, 1000);
        }

        if(!window.cc){
            ENGINE_LOADING = true;
            initEngine();

            //解决ios chrome屏幕下半部无法点击的问题
            if(document.referrer != location.href && (DEVICE.isChrome || DEVICE.isIPhoneXSafari())){
                setTimeout(() => {
                    location.href = location.href;
                }, 500);
            }
        }
    }
}

/**
 * 横屏提示界面布局
 */
function alignLandScapeTips(){
    var landScapeGif = document.getElementById('landscape-gif');
    var landScapeTips = document.getElementById('landscape-tips');
    var landScapeButton = document.getElementById('landscape-back-button');

    landScapeGif.style.left = (window.innerWidth - landScapeGif.width)/2 + 'px';
    landScapeGif.style.top = (window.innerHeight - landScapeGif.height)/2 - parseInt(landScapeTips.style.height)*3 + 'px';

    landScapeTips.style.left = (window.innerWidth - parseInt(landScapeTips.style.width))/2 + 'px';
    landScapeTips.style.top = parseFloat(landScapeGif.style.top) + landScapeGif.height + 'px';

    landScapeButton.style.left = (window.innerWidth - parseInt(landScapeButton.style.width))/2 + 'px';
    landScapeButton.style.top = parseFloat(landScapeTips.style.top) + parseInt(landScapeButton.style.height)*2 + 'px';

    document.getElementById('landscape-tips-div').style.width = window.innerWidth + 'px';
    document.getElementById('landscape-tips-div').style.height = window.innerHeight + 'px';
}

/**
 * 返回游戏列表
 */
function backToGameList(){
    location.href = top.WEB_SITE + getGamePage();
}

/**
 * 加载引擎
 */
function initEngine(){
    // open web debugger console
    if (typeof VConsole !== 'undefined') {
        window.vConsole = new VConsole();
    }

    var cocos2d = document.createElement('script');
    cocos2d.async = true;
    cocos2d.src = window._CCSettings.debug ? 'cocos2d-js.js' : 'cocos2d-js-min.js';

    var engineLoaded = function () {
        if(!window.cc || !window.cc.logID){
            location.href = location.href;
            return;
        }
        ENGINE_LOADED = true;
        document.body.removeChild(cocos2d);
        cocos2d.removeEventListener('load', engineLoaded, false);
        window.boot();
    };
    cocos2d.addEventListener('load', engineLoaded, false);
    document.body.appendChild(cocos2d);
}

/**
 * @description 获取url参数
 */
function getUrlParams(name) {   
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");   
  var r = window.location.search.substr(1).match(reg);   
  if (r != null) return r[2]; return null;  
}

top.SESSION_ID = getUrlParams('sessionId');
top.GAME_ID = getUrlParams('gameCode');
top.STATION_MARK = getUrlParams('tenantCode');
top.WEB_SITE = getUrlParams('webSite') || document.referrer || window.location.origin;
top.SOURCE_ID = getUrlParams('sourceId');
top.LANGUAGE = getUrlParams('lang');

//参数不全跳转到登陆页面
if(!top.SESSION_ID || !top.GAME_ID || !top.STATION_MARK || !top.SOURCE_ID){
    location.href = top.WEB_SITE + '/login';
}

top.WEB_ENTRANCE = true;

top.WSLoader = null;
top.WSLoader_Game = null;

top.WSFrame = {};
top.WSFrame.game = null;
top.WSFrame.hall = null;
top.WSFrame.getInstance = function(url, type){
    if(top.WSFrame[type] == null || top.WSFrame[type].readyState == WebSocket.CLOSED){
        top.WSFrame[type] = new WebSocket(url); 
    }
    return top.WSFrame[type];
}

var GAME_PAGE_SUFFIX = {
    '1' : '/chessHall',
    '2' : '/gamesList',
    '3' : '/gamesList'
}

function getGamePage(){
    if(top.SOURCE_ID){
       return GAME_PAGE_SUFFIX[top.SOURCE_ID];
    }
    return '/gamesList';
}

top.ShowAlert = function(_msg){
    alert(_msg);
    location.href = top.WEB_SITE + getGamePage();
}

top.ShowMsg = function(_msg){
    alert(_msg);
}

/**
 * 游戏加载提示动画
 */
function tipsAnimation(){
    var tips = document.getElementById('tips');
    var dots = '';
    setInterval(() => {
        if(dots.length == 3){
            dots = '';
        }else{
            dots += '.';
        }
        tips.innerHTML = localStr(top.LANGUAGE, '0001') + dots;
    }, 500);
}

tipsAnimation();

/**
 * 显示区域限制图片
 */
top.ShowAreaLimit = function(){
    document.getElementById('area-limit-div').style.display = 'block';
}

function setAreaLimitImg(path, type){
    document.getElementById('area-limit-div').style.background = 'url(' + path + '/' + top.LANGUAGE + '.jpg) no-repeat center center';
    document.getElementById('area-limit-div').style.backgroundSize = '100% 100%';
}

document.getElementById('landscape-back-button').innerHTML = localStr(top.LANGUAGE, '0002');
document.getElementById('landscape-tips').innerHTML = localStr(top.LANGUAGE, '0003');

if(top.SOURCE_ID == '1'){
    setAreaLimitImg('pc_no_service');
}else{
    if (window.orientation == 180 || window.orientation == 0) { //竖屏
        setAreaLimitImg('mb_no_service');
    }
    if (window.orientation == 90 || window.orientation == -90) { //横屏
        setAreaLimitImg('pc_no_service');
    }
}
