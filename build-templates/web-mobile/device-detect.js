var DEVICE = {
    // iPhone X、iPhone XS
    isIPhoneX : /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 3 && window.screen.width === 375 && window.screen.height === 812,
    // iPhone XS Max
    isIPhoneXSMax : /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 3 && window.screen.width === 414 && window.screen.height === 896,
    // iPhone XR
    isIPhoneXR : /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 2 && window.screen.width === 414 && window.screen.height === 896,
    //iPhone 6/6s/7/8
    isIPhone7 : /iphone/gi.test(window.navigator.userAgent) && window.devicePixelRatio && window.devicePixelRatio === 3 && window.screen.width === 375 && window.screen.height === 667,

    //baidu
    isBaidu : navigator.userAgent.match('baidu') != null,
    
    //QQ browser
    isQQ : navigator.userAgent.match('MQQBrowser') != null,
    isQQ9 : navigator.userAgent.match('MQQBrowser/9') != null,
    
    //Saferi Browser
    isFirefox : navigator.userAgent.match('FxiOS') != null,
    
    // UC Browser
    isUC : navigator.userAgent.indexOf("UCBrowser") != -1,
    
    // Chrome 1+
    isChrome : navigator.userAgent.match('CriOS') != null,
    
    //xiaomi
    isXiaomi : navigator.userAgent.match('XiaoMi') != null,

    //Sogou
    isSogou : navigator.userAgent.match('SogouMobileBrowser') != null,

    //360
    is360 : navigator.userAgent.match('QihooBrowser') != null,

    isHuaweiTablet : navigator.userAgent.match('HUAWEIVRD-W09') != null,
    
    // Safari 3.0+ "[object HTMLElementConstructor]"
    isSafari : navigator.userAgent.match('Safari') && !this.isBaidu && !this.isFirefox && !this.isQQ && !this.isChrome && !this.isUC && !this.isXiaomi,

    isIPhoneXSafari : function(){
        return this.isSafari && (this.isIPhoneX || this.isIPhoneXSMax || this.isIPhoneXR);
    },

    isIPhoneXSerise : function(){
        return (this.isIPhoneX || this.isIPhoneXSMax || this.isIPhoneXR);
    },

    iOSBigVersion : function(){
        //获取ios版本号
        var iOSVersionStr = navigator.userAgent.toLowerCase().match(/cpu iphone os (.*?) like mac os/);
        var iOSBigVersion = null;
        if(iOSVersionStr){
            var iOSVersion = iOSVersionStr[1].replace(/_/g,".");
            iOSBigVersion = parseInt(iOSVersion.split('_')[0]);
        }
        return iOSBigVersion;
    },

    isMobile : /(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent)
}

