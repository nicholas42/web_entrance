var CommonNetWork = require('CommonNetWork');
var LoadSceneMgr = require('LoadSceneMgr');
var UIUtil = require('UIUtil');
var Config = require('Config');
var commonConstants = require('commonConstants');
var BaseNetDataNotify = require('BaseNetDataNotify');

module.exports = {

    GAME_PAGE_SUFFIX : {
        '1' : '/chessHall',
        '2' : '/gamesList',
        '3' : '/gamesList'
    },

    getGamePage(){
        if(top.SOURCE_ID){
           return this.GAME_PAGE_SUFFIX[top.SOURCE_ID];
        }
        return '/gamesList';
    },

    onGameExit(_scene){
        switch(_scene){
            case commonConstants.SceneName.Login :
                this.backToMainPage();
                break;
            case commonConstants.SceneName.Hall :
                if(Config.GAME_ROUND_INFO.length > 1){
                    this.backToRoomLayer(false);
                }else{
                    this.backToMainPage();
                }
                break;
            default : break;
        }
    },

    backToRoomLayer(_AutoVerifySafeCode){
        CommonNetWork.CloseAllSocketActions(true,_AutoVerifySafeCode);
        BaseNetDataNotify.ResetSocketStats();
        CommonNetWork.ClearCallback();
        LoadSceneMgr.LoadScene('entrance');
    },

    backToMainPage(){
        CommonNetWork.CloseAllSocketActions(true,false);
        UIUtil.ShowLoading(false);
        top.location.href = Config.WEB_DOMAIN + this.getGamePage();
    },

    backToLoginPage(){
        top.location.href = Config.WEB_DOMAIN + '/login';
    }
}