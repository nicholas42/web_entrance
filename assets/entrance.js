//web版本入口场景
var CommonNetWork = require('CommonNetWork');
var TestNet = require('TestNet');
var Config = require('Config');
var UIUtil = require('UIUtil');
var SubGameConfig = require('SubGameConfig');
var LogUtil = require('LogUtil');
var commonConstants = require('commonConstants');
var PlayerData = require('PlayerDataNotify');
var NewAudioManager = require('NewAudioManager');
var ProtoMapManager = require('ProtoMapManager');
var hallDataNotify = require('hallDataNotify');
var HotForSubGame = require('HotForSubGame');
var SubGameData = require('gameDataNotify');
var DataNotify = require('DataNotify');
var ExitLogic = require('ExitLogic');
var hallManager = require('hallManager');
var localCtr = require("localCtr");

cc.dynamicAtlasManager.enabled = false;
cc.Class({
    extends: hallManager,

    properties: {
        BannerBroadCast : cc.Node
    },

    PreCustomerInit(){
        cc.localCtr = localCtr;
        cc.localCtr.Init();
        UIUtil.SkinFloder = "hallSkin/";

        /**
         * @desciption 如果是从子游戏跳转回来的，就给hallDataNotify重新赋值
         */
        hallDataNotify.m_HallTipsWindow_Switch = null;
        
        NewAudioManager.init();
        ProtoMapManager.Init();

        //覆盖记录上次进入游戏房间的信息的函数
        commonConstants.RememberLastEnterRoomInfo = this.rememberLastEnterRoomInfo.bind(this);
        
        //获取上次进入游戏房间的信息
        var lastEnterRoomInfo = cc.sys.localStorage.getItem('lastEnterRoomInfo');
        if(lastEnterRoomInfo){
            lastEnterRoomInfo = JSON.parse(lastEnterRoomInfo);
            var gameCode = lastEnterRoomInfo.gameCode;
            var roomInfo = lastEnterRoomInfo.roomInfo;
            commonConstants.LastEnterSuccessRoomIndex[gameCode] = roomInfo;
        }
    },

    InitStaticPrefabPath(){

    },
    
    WhenStaticPrefabCreated(){

    },
    
    InitDataNotify()
    {
        this._super();
        hallDataNotify.AddListener('m_RoomLayerPushIn', this.onRoomLayerPushInChange, this);
    },

    CustomerInit () {
        this._m_WindowsArray = []; //弹窗队列数组
        UIUtil.SetLoadingNode(cc.find("CommonLoading"));
        UIUtil.SetTipsNode(cc.find("Tips"));
        UIUtil.ShowLoading(true);

        this.loadConfig();
    },

    CustomerRelease()
    {
        this._super();
        top.WSLoader.UnReg(TestNet.UserData.Receive);
    },

    /**
     * @description 读取配置连接网络
     */
    loadConfig(){
        cc.loader.loadRes('customer_config', cc.JsonAsset, (error, data) => {

            if(error) return;
            this.customer_config = data;

            Config.STATION_MARK = top.STATION_MARK;
            Config.LOCAL_MASTER_VERSION = Config.getLocalVersion();
            var netConfig = this.customer_config.json[this.customer_config.json.netconfig];

            if(cc.sys.isBrowser){
                Config.HTTP_BASE_URL = top.WEB_SITE;
                Config.fileServicePath = top.WEB_SITE;
            }else{
                Config.HTTP_BASE_URL = netConfig.HTTP_BASE_URL;
                Config.fileServicePath = "";
            }

            Config.WEB_DOMAIN = top.WEB_SITE;

            //如果是https的域名，需要调整为wss连接
            var https = top.WEB_SITE.split('/')[0] == 'https:';
            var domain = top.WEB_SITE.split('/')[2];
            var wsUrl = https ? 'wss://' + domain : 'ws://' + domain;

            Config.WEBSOCKET_HALL_URL = wsUrl;
            Config.WEBSOCKET_GAME_URL = wsUrl + '/v1/game/gameGate';

            Config.GAME_ID = top.GAME_ID;
            commonConstants.CurrentGamingID = Config.GAME_ID;
            commonConstants.SessionID = top.SESSION_ID;
            Config.GAME_LUNCH_SCENE = SubGameConfig.getSubGameLoadScene(Config.GAME_ID);
            Config.GAME_SHIELD_URL = netConfig.GAME_SHIELD_URL;

            cc.LogUtil = LogUtil;
            cc.LogUtil.SetEnv(this.customer_config.json.netconfig);

            CommonNetWork.Init();
            this.takeTheCommonNetWorkCallbacks();
            CommonNetWork.ConnectSocket();
            top.WSLoader.Reg(TestNet.UserData.Receive , this.onReceiveUserData, this);
        });
    },

    /**
     * @description 接管网络回调
     */
    takeTheCommonNetWorkCallbacks(){
        CommonNetWork.ConnectSuccessCallBack = this.onConnectSuccess.bind(this);
        CommonNetWork.OnGameEnterCallback = this.onGameEnterCallback.bind(this);
        CommonNetWork.OnOtherUserLoginCallback = this.onOtherUserLoginCallback.bind(this);
        CommonNetWork.OnHallLoginErrorCallback = this.onHallLoginErrorCallback.bind(this);
        //CommonNetWork.OnGateError = this.onGateError.bind(this);
        CommonNetWork.OnOtherErrorCallback = this.onOtherError.bind(this);
        CommonNetWork.BackToLoginScene = ExitLogic.onGameExit.bind(ExitLogic, commonConstants.SceneName.Login);
        CommonNetWork.BackToHallScene = ExitLogic.onGameExit.bind(ExitLogic, commonConstants.SceneName.Hall);
    },

    /**
     * @description 生成子游戏管理器实例
     */
    generateSubGameManager(){
        for(var i=0; i<SubGameConfig.SUB_GAME_LOADSCENE.length; i++){
            SubGameData.prototype = new DataNotify();
            var subGameData = new SubGameData();

            var gameId = SubGameConfig.SUB_GAME_LOADSCENE[i].gameId;

            var gameInfo = hallDataNotify.GetGameInfoByCode(gameId);

            if(gameInfo){
                new HotForSubGame(gameInfo, subGameData);
            }
        }
    },

    /**
     * @description 网络连接成功回调
     * @param {*} _gameCode 
     */
    onConnectSuccess(_gameCode){
        
        this.generateSubGameManager();

        hallDataNotify.m_SubgameManager = HotForSubGame.getSubGameManager(Config.GAME_ID);

        Config.GAME_ROUND_INFO = hallDataNotify.GetRoomDataByGameCode(Config.GAME_ID);

        if(Config.GAME_ROUND_INFO.length == 1)
        {
            //直接进入游戏
            commonConstants.CurrentGamingID = Config.GAME_ID;
            hallDataNotify.m_SubgameManager.enterGame(Config.GAME_ROUND_INFO[0]);
        }
        else
        {
            top.SPLASH.style.display = 'none';
            this.BannerBroadCast.active = true;
            //弹出选场
            commonConstants.CurrentGamingID = Config.GAME_ID;
            hallDataNotify.m_RoomLayerIndex = Config.GAME_ID;
        }
    },

    /**
     * @description 房间选择页面退出处理
     */
    onRoomLayerPushInChange(_val, _before){
        if(_val) return;
        ExitLogic.backToMainPage();
    },

    /**
     * @description 异地登陆回调
     */
    onOtherUserLoginCallback(){
        ExitLogic.backToMainPage();
    },

    /**
     * 大厅登陆错误处理
     * @param {*} _data 
     */
    onHallLoginErrorCallback(_data){
        if(_data.code == 4 || _data.code == 8){
            //区域限制
            if(_data.code == 4 && _data.httpCode == -19 && _data.apiCode == 6){
                top.ShowAreaLimit();
                return;
            }
            this.MSG_PRE = true;
            top.ShowAlert(_data.msg);
        }  
    },

    /**
     * @description 登陆异常
     */
    onGateError(_data){
        if(this.MSG_PRE) return;
        top.ShowMsg(_data);
        ExitLogic.backToLoginPage();
    },

    /**
     * @description 其他异常
     * @param {*} _data 
     */
    onOtherError(_data, _errMsg){
        top.ShowAlert(_errMsg);
    },

    /**
     * @description 进入游戏成功回调
     */
    onGameEnterCallback(){
        UIUtil.ShowLoading(false);
    },

    /**
     * @description 获取到用户信息回调
     * @param {*} _data  
     */
    onReceiveUserData(_data){
        // 个人配置信息
        var userInfo = JSON.parse(_data.userInfo);
        PlayerData.SetVipConfig(userInfo.usersGradeVoList);
        PlayerData.SetPlayerData(userInfo.userInfoForFront);

        let balanceInfo = JSON.parse(_data.balanceInfo);
        PlayerData.m_Money = parseFloat(UIUtil.ConvertNumToPoint2(balanceInfo.balance));
        PlayerData.m_SafeMoney = balanceInfo.safeBalance;

        if (_data.securityDistributeUrlData) 
        {
            let securityUrls = JSON.parse(_data.securityDistributeUrlData);
            if (Array.isArray(securityUrls) && securityUrls.length > 0) 
            {
                let securityStr = securityUrls.join(",");
                UIUtil.setValueForSecretFile(Config.Keys.DismissKey, securityStr);
            }
        }

        var gameInfo = JSON.parse(_data.gameInfo);//游戏场次列表
        hallDataNotify.DealGameInfoList(gameInfo);
    },

    /**
     * @description 记录上次进入游戏房间的信息
     * @param {*} _gameCode 
     * @param {*} _roomInfo 
     */
    rememberLastEnterRoomInfo(_gameCode, _roomInfo){
        var lastEnterRoomInfo = JSON.stringify({
            gameCode : _gameCode,
            roomInfo : _roomInfo
        });
        cc.sys.localStorage.setItem('lastEnterRoomInfo', lastEnterRoomInfo);
        commonConstants.LastEnterSuccessRoomIndex[_gameCode] = _roomInfo;
    }
});
